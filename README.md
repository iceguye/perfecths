# PerfectHS

## DESCRIPTION

PerfectHS is a set of programs to perform a series of calculations and
conversions for samples of piano sounds, to make their overtones
(harmonic series) to become "Perfect Harmonic Series".

Perfect Harmonic Series is a result that all the notes in the
overtones (harmonic series) are equal temperament. See [here for more
details](https://en.wikipedia.org/wiki/Harmonic_series_(music)#Frequencies,_wavelengths,_and_musical_intervals_in_example_systems).

## Minimal System Requirements

Posix compatible system (currently only Linux being tested).

16 GB free disk space.

16 GB memory.

## DEPENDENCIES

python>=3.7.5

pydub>=0.23.1

sox>=14.4.2.0

numpy>=1.20.1

scipy>=1.6.2

If you are using a system which is not Unix-like and having issues of
running sox, you should try to modify the all the sox_cmd variables or
os.system function calls.

## AUTHORS

IceGuye

  -- Main developer

Alexander Holm

  -- Who provided the samples freely, email: axeldenstore (at) gmail
     (dot) com

Hao

  -- Physicist, provided huge supports and contributions to the
     program of perfect harmonic series.

## USAGE

### Use Perfect Harmonic Series

    python3 perfecths.py A4_FREQUENCY CPU_THREADS

For example:
    
    python3 perfecths.py 440 16

WARNING: This will take a lot of system resource, and it takes very
long time to be done depending on your computer specs.

    python3 process_perfecths_samples.py

The processing time is probably a little longer than what you
expected, please be patient.

### Non Perfect Harmonic Series Samples Processing

If you don't want to use Perfect Harmonic Series samples, but just
want the regular samples for IcePiano-R, you can simply run this:

    python process_normal_samples.py

The processing time is probably a little longer than what you
expected, please be patient.

## CONTACTS

ice.guye@bk.ru

## COPYRIGHT

Copyright © 2020-2021 IceGuye,

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.